import sys
import os
from glob import glob
from collections import defaultdict

def read_file(path):
	hamfiles, spamfiles = [],[]
	for x in os.walk(path):
		for y in glob(os.path.join(x[0], '*.txt')):
			if x[0][-3:] == "ham":
				hamfiles.append(y)
			elif x[0][-4:] == "spam":
				spamfiles.append(y)
	return hamfiles, spamfiles


        
	

def main():
	path = sys.argv[1]
	hamfiles, spamfiles = read_file(path)
	total_words = 0 # count of words in hams and spams
	spam_total_words, ham_total_words = 0,0
	spam_dict, ham_dict = defaultdict(lambda: 0),defaultdict(lambda: 0)
	hams,spams = [],[]

	for file in hamfiles:
		f = open(file, "r", encoding="latin1")
		for line in f.readlines():
			line = list(filter(lambda x:x!='', line.strip().split(' ')))
			line = " ".join(line).split(' ')
			hams.append(line)
			ham_total_words += len(line)
		f.close()

	for file in spamfiles:
		f = open(file, "r", encoding="latin1")
		for line in f.readlines():
			line = list(filter(lambda x:x!='', line.strip().split(' ')))
			line = " ".join(line).split(' ')
			# newline = []
			# for x in line:
			# 	x = x.encode("latin1","ignore").decode("latin1")
			# 	if len(x)>0:
			# 		newline.append(x)
			# line = newline
			spams.append(line)
			spam_total_words += len(line)
		f.close()

	total_words = ham_total_words + spam_total_words

	p_spam = float(spam_total_words/total_words)
	p_ham = float(ham_total_words/total_words)

	# p_token_ham and p_token_spam
	for line in spams:
		for token in line:
			spam_dict[token]+=1
	for line in hams:
		for token in line:
			ham_dict[token]+=1
	vocabs = list(set(list(spam_dict.keys())+list(ham_dict.keys())))
	# for i in vocabs:d
	# 	print(i)
	vocab_size = len(vocabs)
	p_token_class = [[0.0 for i in range(2)] for j in range(vocab_size)]

	# add one smoothing
	smoothing = 1
	for i in range(vocab_size):
		p_token_class[i][0] = float((spam_dict[vocabs[i]]+smoothing)/(spam_total_words+smoothing*vocab_size))
		p_token_class[i][1] = float((ham_dict[vocabs[i]]+smoothing)/(ham_total_words+smoothing*vocab_size))

	# write to nbmodel
	f = open("nbmodel.txt", "w")
	f2 = open("vocab.txt", "w", encoding="latin1")
	f.write(str(p_spam) + ' ' + str(p_ham) + '\n')
	for i in range(vocab_size):
		if i == vocab_size-1:
			line =  vocabs[i]+' '+ str(p_token_class[i][0])+' '+str(p_token_class[i][1])
			f.write(line)
		else:
			line = vocabs[i]+' '+ str(p_token_class[i][0])+' '+str(p_token_class[i][1])+'\n'
			f.write(line)
		f2.write(vocabs[i]+'\n')
	f.close()
	f2.close()




if __name__ == '__main__':
	main()