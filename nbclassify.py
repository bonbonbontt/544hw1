#todo: encoding problem
from collections import defaultdict
import os
from glob import glob
import sys

def read_from_model(model_path):
	f = open(model_path, "r", encoding="latin1")
	data = []
	for line in f.readlines():
		# line = line.encode("latin-1","ignore")
		newline = line.strip().split(' ')
		# if len(newline)==2:
		# 	print(line)
		data.append(newline)
	f.close()
	p_spam, p_ham = float(data[0][0]), float(data[0][1])
	vocabs = []
	token_spam_dict, token_ham_dict = defaultdict(lambda: 1),defaultdict(lambda: 1)
	for line in data[1:]:
		# print(line)
		token = line[0]
		vocabs.append(token)
		token_spam_dict[token] = float(line[1])
		token_ham_dict[token] = float(line[2])
	return p_spam,p_ham,vocabs, token_spam_dict, token_ham_dict

def process_file(path):
	f = open(path, "r", encoding="latin1")
	msg = [] 
	for line in f.readlines():
		line = list(filter(lambda x:x!='', line.strip().split(' ')))
		msg += line
	f.close()
	return msg

def main():
	# model_path = "./fakemodel.txt"
	model_path = "./nbmodel.txt"
	# model_path = './trial3_model.txt'
	p_spam,p_ham,vocabs, token_spam_dict, token_ham_dict = read_from_model(model_path)
	path = sys.argv[1]
	txtfiles = [y for x in os.walk(path) for y in glob(os.path.join(x[0], '*.txt'))]
	f = open("./nboutput.txt","w", encoding="latin1")
	for file in txtfiles:
		msg = process_file(file)
		p_msg_spam, p_msg_ham = 1.0,1.0
		for token in msg:
			p_msg_spam *= token_spam_dict[token]
			p_msg_ham *= token_ham_dict[token]
		p_msg_spam *= p_spam
		p_msg_ham *= p_ham
		label = "spam" if p_msg_spam>p_msg_ham else "ham"
		f.write(label + ' ' + file)
		if file !=txtfiles[-1]:
			f.write("\n")
	f.close()



if __name__ == '__main__':
	main()